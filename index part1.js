const express = require('express')
const app = express()
const anyid = require('anyid').anyid;
var morgan = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose')
const password = process.argv[2]


const url = `mongodb+srv://ccerero1612:${password}@cluster0.r606sn7.mongodb.net/PhoneBookApp?retryWrites=true&w=majority&appName=Cluster0`


mongoose.set('strictQuery',false)

mongoose.connect(url)

const noteSchema = new mongoose.Schema({
  content: String,
  important: Boolean,
})

const Note = mongoose.model('Note', noteSchema)

app.use(express.json())
app.use(cors())
app.use(express.static('dist')) 

morgan.token('TokenData', function getId (req) {
 return JSON.stringify(req.body)
})

app.use(morgan(':method :url :status :res[content-length] - :response-time ms :TokenData')); 

let notes = [
  {
    id: 1,
    content: "HTML is easy",
    important: true
  },
  {
    id: 2,
    content: "Browser can execute only JavaScript",
    important: false
  },
  {
    id: 3,
    content: "GET and POST are the most important methods of HTTP protocol",
    important: true
  }
]

let persons = [
  { 
    "id": 1,
    "name": "Arto Hellas", 
    "number": "040-123456"
  },
  { 
    "id": 2,
    "name": "Ada Lovelace", 
    "number": "39-44-5323523"
  },
  { 
    "id": 3,
    "name": "Dan Abramov", 
    "number": "12-43-234345"
  },
  { 
    "id": 4,
    "name": "Mary Poppendieck", 
    "number": "39-23-6423122"
  }
]

app.get('/', (request, response) => {
  response.send('<h1>Hello World!</h1>')
})



// PERSONS
app.get('/api/persons', (request, response) => {

  response.json(persons)
})

app.get('/api/persons/info', (request, response) => {
  let info  = ''
  const dateNow = new Date();
  info = `Server running on port ${persons.length} people  ${dateNow}`
  response.json(info)
})

app.get('/api/persons/:id', (request, response) => {
  const id = Number(request.params.id)
  const note = notes.find(note => {
    console.log(note.id, typeof note.id, id, typeof id, note.id === id)
    return note.id === id
  })

  if(note){
    response.json(note)
  }else{
    let message = {
      "message" : "El contacto no existe",
      "status" : "Error"
    }
    response.status(404).json(message).end()
  }
 
})

app.delete('/api/persons/:id', (request, response) => {
  const id = Number(request.params.id)
  data = persons.filter(note => note.id !== id)


  let message = {
    "message" : "El contacto eliminado",
    "status" : "Success",
    "content" : data
  }
  response.status(200).json(message).end()
})

const generateIdPerson = () => {
  const index = anyid().encode('Aa0').length(6).random().id();
  
  return index 
}


app.post('/api/persons', (request, response) => {
  const body = request.body
  let status = false

  if (body) {
    let message = {
      "status" : "Error",
      "message" : "Sin valores"
    }

  
    if(!body.hasOwnProperty("name")){
      message = {
        ...message,
        message : "name empty"
      }
      status = true
    }

    if(!body.hasOwnProperty("number")){
      message = {
        ...message,
        message : "number empty"
      }
      status = true
    }

    if(status){
      return response.status(400).json(message)
    }

    let person = {
      name: body.name,
      number: body.number,
      id: generateIdPerson(),
    }

    persons = persons.concat(person)

     message = {
      "message" : "El contacto registrado",
      "status" : "Success",
      "content" : persons
    }

    response.json(message)
  
  }

})

// NOTES

app.get('/api/notes', (request, response) => {
  response.json(notes)
})

app.get('/api/notes/:id', (request, response) => {
  const id = Number(request.params.id)
  const note = notes.find(note => {
    console.log(note.id, typeof note.id, id, typeof id, note.id === id)
    return note.id === id
  })

  if(note){
    response.json(note)
  }else{
    response.status(404).end()
  }
 
})

app.delete('/api/notes/:id', (request, response) => {
  const id = Number(request.params.id)
  notes = notes.filter(note => note.id !== id)

  response.status(204).end()
})

const generateId = () => {
  const maxId = notes.length > 0
    ? Math.max(...notes.map(n => n.id))
    : 0
  return maxId + 1
}

app.post('/api/notes', (request, response) => {
  const body = request.body

  if (!body.content) {
    return response.status(400).json({ 
      error: 'content missing' 
    })
  }

  const note = {
    content: body.content,
    important: Boolean(body.important) || false,
    id: generateId(),
  }

  notes = notes.concat(note)

  response.json(notes)
})


const unknownEndpoint = (request, response) => {
  response.status(404).send({ error: 'unknown endpoint' })
}

app.use(unknownEndpoint)

const PORT = process.env.PORT || 3001
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})