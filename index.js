require('dotenv').config()
const express = require('express')
const app = express()
const cors = require('cors')
const Phone = require('./models/phonebook')


app.use(express.static('dist')) 

const requestLogger = (request, response, next) => {
  console.log('Method:', request.method)
  console.log('Path:  ', request.path)
  console.log('Body:  ', request.body)
  console.log('---')
  next()
}

const errorHandler = (error, request, response, next) => {
  console.error(error.message)
  let arg = error.errors;
  // let message ={}
  // for (const key in arg) {
  //   if (Object.hasOwnProperty.call(arg, key)) {
  //     const element = arg[key];

  //     message = {
  //       properties : element.path,
  //       message : element.message
  //     }
  //   }
  // }


  if (error.name === 'CastError') {
    return response.status(400).send({ error: 'malformatted id' })
  }else if (error.name === 'ValidationError') {
    return response.status(400).json({ error: arg })
  }

  next(error)
}

app.use(express.json())
app.use(cors())
app.use(requestLogger)

const unknownEndpoint = (request, response) => {
  response.status(404).send({ error: 'unknown endpoint' })
}

app.get('/', (request, response) => {
  response.send('<h1>Hello World!</h1>')
})

app.get('/api/persons', (request, response) => {

   let filters = request.query.filters ? request.query.filters : {}
  Phone.find(filters).then(notes => {
    response.json(notes)
  })
})

app.post('/api/persons', (request, response,next) => {
  const body = request.body

  // if (body.name === undefined || body.number === undefined) {
  //   return response.status(400).json({ error: 'content missing' })
  // }

  const arg = new Phone({
    name: body.name,
    number: body.number,
  })

  arg.save().then(savedNote => {
    response.json(savedNote)
  })  .catch(error => next(error))
})


app.get('/api/persons/:id', (request, response) => {
  Phone.findById(request.params.id).then(note => {
    response.json(note)
  })
})

app.delete('/api/persons/:id', (request, response, next) => {
  Phone.findByIdAndDelete(request.params.id)
    .then(result => {
      response.status(204).end()
    })
    .catch(error => next(error))
})

app.put('/api/persons/:id', (request, response, next) => {
  const body = request.body

  const person = {
    name: body.name,
    number: body.number,
  }

  Phone.findByIdAndUpdate(request.params.id, person, { new: true })
    .then(updatedNote => {
      response.json(updatedNote)
    })
    .catch(error => next(error))
})



app.use(unknownEndpoint)
app.use(errorHandler)

const PORT = process.env.PORT
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})